package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name = "! wait 15s, forward 1s", group = "Competition")
//@Disabled
public class omnautow15 extends LinearOpMode {
    public double none = 0.0;
    public double left = -1.0;
    public double right = 1.0;
    public double backward = -1.0;
    public double forward = 1.0;
    public double turnLeft = -1;
    public double turnRight = 1;
    public boolean done = false;
    /* Declare OpMode members. */
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor topLeft = null;
    private DcMotor topRight = null;
    private DcMotor bottomLeft = null;
    private DcMotor bottomRight = null;

    public void doTelemetry() {
        telemetry.addData("topLeft", topLeft.getPower());
        telemetry.addData("topRight", topRight.getPower());
        telemetry.addData("bottomLeft", bottomLeft.getPower());
        telemetry.addData("bottomRight", bottomRight.getPower());

        telemetry.addData("topLeft mode", topLeft.getMode());
        telemetry.addData("topRight mode", topRight.getMode());
        telemetry.addData("bottomLeft mode", bottomLeft.getMode());
        telemetry.addData("bottomRight mode", bottomRight.getMode());

        telemetry.addData("topLeft position", topLeft.getCurrentPosition() +
                " (target " +
                topLeft.getTargetPosition()
                + ")");
        telemetry.addData("topRight position", topRight.getCurrentPosition() +
                " (target " +
                topRight.getTargetPosition()
                + ")");
        telemetry.addData("bottomLeft position", bottomLeft.getCurrentPosition() +
                " (target " +
                bottomLeft.getTargetPosition()
                + ")");
        telemetry.addData("bottomRight position", bottomRight.getCurrentPosition() +
                " (target " +
                bottomRight.getTargetPosition()
                + ")");

        telemetry.update();
    }

    public void setDirection(Double x, Double y, Double rotation) {
        // https://ftcforum.usfirst.org/forum/ftc-technology/android-studio/6361-mecanum-wheels-drive-code-examp
        double r = Math.hypot(x, y * -1);
        double robotAngle = Math.atan2(y, x) - Math.PI / 4;
        double rightX = rotation;
        final double v1 = r * Math.cos(robotAngle) + rightX;
        final double v2 = r * Math.sin(robotAngle) - rightX;
        final double v3 = r * Math.sin(robotAngle) + rightX;
        final double v4 = r * Math.cos(robotAngle) - rightX;

        topLeft.setPower(v1);
        topRight.setPower(v2);
        bottomLeft.setPower(v3);
        bottomRight.setPower(v4);

        doTelemetry();
    }

    public void goTime(Double x, Double y, int ms, Double rotation) {
        telemetry.addData("X", x);
        telemetry.addData("Y", y);
        telemetry.addData("Time", ms);
        telemetry.addData("Rotation", rotation);
        telemetry.update();
        setDirection(x, y, rotation);
        sleep(ms);
        setDirection(none, none, none);
    }
    /*
    public void goTicks(Double x, Double y, int ticks, Double rotation) {
        setDirection(x, y, rotation);
        sleep(ms);
        setDirection(none, none, none);
    }
    */

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");

        /* eg: Initialize the hardware variables. Note that the strings used here as parameters
         * to 'get' must correspond to the names assigned during the robot configuration
         * step (using the FTC Robot Controller app on the phone).
         */
        topLeft = hardwareMap.dcMotor.get("topLeft");
        topRight = hardwareMap.dcMotor.get("topRight");
        bottomLeft = hardwareMap.dcMotor.get("bottomLeft");
        bottomRight = hardwareMap.dcMotor.get("bottomRight");

        topLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        topRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        bottomLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        bottomRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        // eg: Set the drive motor directions:
        // Reverse the motor that runs backwards when connected directly to the battery
        topLeft.setDirection(DcMotor.Direction.FORWARD);
        topRight.setDirection(DcMotor.Direction.REVERSE);
        bottomLeft.setDirection(DcMotor.Direction.FORWARD);
        bottomRight.setDirection(DcMotor.Direction.REVERSE);

        telemetry.addData("Status 1", "Initialized");

        telemetry.addData("Status 2", "Running Nov 7 ver: " + runtime.toString());

        telemetry.addData("Status 3", "Ready to run");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        while (opModeIsActive() && !done) {
            // button names here: http://ftckey.com/apis/ftc/index.html?com/qualcomm/robotcore/hardware/Gamepad.htmlle

            goTime(none, none, 15000, none); // wait 5 sec
            goTime(none, forward, 1000, none); // forward 2 sec
            goTime(none, none, 0, none); // stop at the end

            telemetry.update();
            idle();
            done = true;
        }

        telemetry.addData("Path", "Complete");
        telemetry.update();
    }
}