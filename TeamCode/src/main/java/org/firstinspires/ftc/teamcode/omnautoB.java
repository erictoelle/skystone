package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

@Autonomous(name = "Maia Shuffle (omnautoB route 1)", group = "Competition")
//@Disabled
public class omnautoB extends LinearOpMode {
    public boolean done = false;
    /* Declare OpMode members. */
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor topLeft = null;
    private DcMotor topRight = null;
    private DcMotor bottomLeft = null;
    private DcMotor bottomRight = null;
    private DcMotor Slide = null;

    //private CRServo Gripper = null;

    public void doTelemetry() {
        telemetry.addData("topLeft", topLeft.getPower());
        telemetry.addData("topRight", topRight.getPower());
        telemetry.addData("bottomLeft", bottomLeft.getPower());
        telemetry.addData("bottomRight", bottomRight.getPower());

        telemetry.addData("topLeft mode", topLeft.getMode());
        telemetry.addData("topRight mode", topRight.getMode());
        telemetry.addData("bottomLeft mode", bottomLeft.getMode());
        telemetry.addData("bottomRight mode", bottomRight.getMode());

        telemetry.addData("topLeft position", topLeft.getCurrentPosition() +
                " (target " +
                topLeft.getTargetPosition()
                + ")");
        telemetry.addData("topRight position", topRight.getCurrentPosition() +
                " (target " +
                topRight.getTargetPosition()
                + ")");
        telemetry.addData("bottomLeft position", bottomLeft.getCurrentPosition() +
                " (target " +
                bottomLeft.getTargetPosition()
                + ")");
        telemetry.addData("bottomRight position", bottomRight.getCurrentPosition() +
                " (target " +
                bottomRight.getTargetPosition()
                + ")");

        telemetry.update();
    }

    public void goLeft(int ms) {
        telemetry.addData("Strafing left", ms);
        topLeft.setPower(-0.5);
        topRight.setPower(0.5);
        bottomLeft.setPower(0.5);
        bottomRight.setPower(-0.5);
        telemetry.update();
        sleep(ms);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void goRight(int ms) {
        telemetry.addData("Strafing right", ms);
        topLeft.setPower(0.5);
        topRight.setPower(-0.5);
        bottomLeft.setPower(-0.5);
        bottomRight.setPower(0.5);
        telemetry.update();
        sleep(ms);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void goForward(int ms) {
        telemetry.addData("Going", "forward");
        telemetry.update();
        topLeft.setPower(0.5);
        topRight.setPower(0.5);
        bottomLeft.setPower(0.5);
        bottomRight.setPower(0.5);
        sleep(ms);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void goBackward(int ms) {
        telemetry.addData("Going", "backward");
        telemetry.update();
        topLeft.setPower(-0.5);
        topRight.setPower(-0.5);
        bottomLeft.setPower(-0.5);
        bottomRight.setPower(-0.5);
        sleep(ms);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void goForwardLeft(int ms) {
        telemetry.addData("Going", "backward");
        telemetry.update();
        topLeft.setPower(0);
        topRight.setPower(0.5);
        bottomLeft.setPower(0.5);
        bottomRight.setPower(0);
        sleep(ms);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void goClockwise(int ms) {
        telemetry.addData("Going", "clockwise");
        telemetry.update();
        topLeft.setPower(0.5);
        topRight.setPower(-0.5);
        bottomLeft.setPower(0.5);
        bottomRight.setPower(-0.5);
        sleep(ms);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void slideUp(int ms) {
        telemetry.addData("Slide", "up");
        telemetry.update();
        Slide.setPower(-0.5);
        sleep(ms);
        Slide.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void slideDown(int ms) {
        telemetry.addData("Slide", "down");
        telemetry.update();
        Slide.setPower(0.25);
        sleep(ms);
        Slide.setPower(0);
        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    public void slideDownAndRight(int ms, int rightms) {
        telemetry.addData("Slide", "down and Going right");
        telemetry.update();

        int downextrams = ms - rightms;

        topLeft.setPower(0.5);
        topRight.setPower(-0.5);
        bottomLeft.setPower(-0.5);
        bottomRight.setPower(0.5);

        Slide.setPower(0.25);
        sleep(rightms);
        Slide.setPower(0);

        sleep(downextrams);
        topLeft.setPower(0);
        topRight.setPower(0);
        bottomLeft.setPower(0);
        bottomRight.setPower(0);

        telemetry.addData("Waiting", "...");
        telemetry.update();
    }

    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");

        /* eg: Initialize the hardware variables. Note that the strings used here as parameters
         * to 'get' must correspond to the names assigned during the robot configuration
         * step (using the FTC Robot Controller app on the phone).
         */
        topLeft = hardwareMap.dcMotor.get("topLeft");
        topRight = hardwareMap.dcMotor.get("topRight");
        bottomLeft = hardwareMap.dcMotor.get("bottomLeft");
        bottomRight = hardwareMap.dcMotor.get("bottomRight");
        Slide = hardwareMap.dcMotor.get("LinearSlide");
        //Gripper = hardwareMap.crservo.get("Gripper");

        topLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        topRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        bottomLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        bottomRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        // eg: Set the drive motor directions:
        // Reverse the motor that runs backwards when connected directly to the battery
        topLeft.setDirection(DcMotor.Direction.FORWARD);
        topRight.setDirection(DcMotor.Direction.REVERSE);
        bottomLeft.setDirection(DcMotor.Direction.FORWARD);
        bottomRight.setDirection(DcMotor.Direction.REVERSE);

        telemetry.addData("Status 1", "Initialized");

        telemetry.addData("Status 2", "Running Jan 13 ver: " + runtime.toString());

        telemetry.addData("Status 3", "Ready to run");    //
        telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        while (opModeIsActive() && !done) {
            // AAAAAAAAAAAAAAAAAAAAA

            slideUp(1250);
            sleep(1000);
            //slideDown(750);
            goRight(1000);
            sleep(1000);

            //slideDown(750);
            //sleep(1000);

            slideDownAndRight(900, 100);
            sleep(1000);

            goLeft(2000);
            sleep(1000);

            // MAIA SHUFFLE GOES HERE

            /* OPTION 1
            ________          __  .__                 ____
            \_____  \ _______/  |_|__| ____   ____   /_   |
             /   |   \\____ \   __\  |/  _ \ /    \   |   |
            /    |    \  |_> >  | |  (  <_> )   |  \  |   |
            \_______  /   __/|__| |__|\____/|___|  /  |___|
                    \/|__|                       \/
             */
            /*
            goRight(4000);

            sleep(1000); // wait 1 sec

            goBackward(3000);
            sleep(1000); // wait 1 sec

            goClockwise(1000);
            /*
            ________          __  .__                ________
            \_____  \ _______/  |_|__| ____   ____   \_____  \
             /   |   \\____ \   __\  |/  _ \ /    \   /  ____/
            /    |    \  |_> >  | |  (  <_> )   |  \ /       \
            \_______  /   __/|__| |__|\____/|___|  / \_______ \
                    \/|__|                       \/          \/
             */

            /*
            goClockwise(1000);
            sleep(1000);
            goForward(1000);
            */

            telemetry.update();
            idle();
            done = true;
        }

        telemetry.addData("Path", "Complete");
        telemetry.update();
    }
}