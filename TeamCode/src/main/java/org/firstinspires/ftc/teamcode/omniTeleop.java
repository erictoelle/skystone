package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;


/**
 * This file contains an example of an iterative (Non-Linear) "OpMode".
 * An OpMode is a 'program' that runs in either the autonomous or the teleop period of an FTC match.
 * The names of OpModes appear on the menu of the FTC Driver Station.
 * When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 * <p>
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all iterative OpModes contain.
 * <p>
 * Use Android Studios to Copy this Class, and Paste it into your team's code folder with a new name.
 * Remove or comment out the @Disabled line to add this opmode to the Driver Station OpMode list
 */

@TeleOp(name = "! Omniwheels", group = "Competition")
// @Autonomous(...) is the other common choice
//@Disabled
public class omniTeleop extends OpMode {
    /* Declare OpMode members. */
    private ElapsedTime runtime = new ElapsedTime();

    private DcMotor topLeft = null;
    private DcMotor topRight = null;
    private DcMotor bottomLeft = null;
    private DcMotor bottomRight = null;
    private DcMotor Slide = null;
    private CRServo Gripper = null;

    private double speedMultiplier = 1;


    /*
     * Code to run ONCE when the driver hits INIT
     */
    @Override
    public void init() {
        telemetry.addData("Status", "Initialized");

        /* eg: Initialize the hardware variables. Note that the strings used here as parameters
         * to 'get' must correspond to the names assigned during the robot configuration
         * step (using the FTC Robot Controller app on the phone).
         */
        topLeft = hardwareMap.dcMotor.get("topLeft");
        topRight = hardwareMap.dcMotor.get("topRight");
        bottomLeft = hardwareMap.dcMotor.get("bottomLeft");
        bottomRight = hardwareMap.dcMotor.get("bottomRight");
        Gripper = hardwareMap.crservo.get("Gripper");
        Slide = hardwareMap.dcMotor.get("LinearSlide");

        // eg: Set the drive motor directions:
        // Reverse the motor that runs backwards when connected directly to the battery
        topLeft.setDirection(DcMotor.Direction.FORWARD);
        topRight.setDirection(DcMotor.Direction.REVERSE);
        bottomLeft.setDirection(DcMotor.Direction.FORWARD);
        bottomRight.setDirection(DcMotor.Direction.REVERSE);

        telemetry.addData("Status", "Initialized");
    }

    /*
     * Code to run REPEATEDLY after the driver hits INIT, but before they hit PLAY
     */
    @Override
    public void init_loop() {
    }

    /*
     * Code to run ONCE when the driver hits PLAY
     */
    @Override
    public void start() {
        runtime.reset();
    }

    public void doSleep(int i) {
        // unused, probably broken
        long setTime = System.currentTimeMillis();
        while ((System.currentTimeMillis() - setTime) < i) {
            return;
        }
    }

    /*
     * Code to run REPEATEDLY after the driver hits PLAY but before they hit STOP
     */
    @Override
    public void loop() {
        telemetry.addData("Status", "Running Nov 6 ver: " + runtime.toString());
        // button names here: http://ftckey.com/apis/ftc/index.html?com/qualcomm/robotcore/hardware/Gamepad.html

		// https://ftcforum.usfirst.org/forum/ftc-technology/android-studio/6361-mecanum-wheels-drive-code-example

        if (gamepad1.right_bumper) {
            speedMultiplier = 0.5;
        }
        if (gamepad1.left_bumper) {
            speedMultiplier = 1;
        }

        double x = gamepad1.left_stick_x * speedMultiplier;
        double y = gamepad1.left_stick_y * speedMultiplier;

        if (gamepad1.dpad_left) {
            x = -1;
        }
        if (gamepad1.dpad_right) {
            y = 1;
        }

        double r = Math.hypot(x, y * -1);
        double robotAngle = Math.atan2(y * -1, x) - Math.PI / 4;
        double rightX = (gamepad1.right_stick_x * (speedMultiplier - 0.2));

        double v1 = r * Math.cos(robotAngle) + rightX;
        double v2 = r * Math.sin(robotAngle) - rightX;
        double v3 = r * Math.sin(robotAngle) + rightX;
        double v4 = r * Math.cos(robotAngle) - rightX;

        topLeft.setPower(v1);
        topRight.setPower(v2);
        bottomLeft.setPower(v3);
        bottomRight.setPower(v4);

        Double triggerSpeed;
        triggerSpeed = 0.25; // was 0.5
        while (gamepad1.right_trigger > 0.1) {
            topLeft.setPower(-triggerSpeed * gamepad1.right_trigger);
            topRight.setPower(triggerSpeed * gamepad1.right_trigger);
            bottomLeft.setPower(-triggerSpeed * gamepad1.right_trigger);
            bottomRight.setPower(triggerSpeed * gamepad1.right_trigger);
        }
        while (gamepad1.left_trigger > 0.1) {
            topLeft.setPower(triggerSpeed * gamepad1.left_trigger);
            topRight.setPower(-triggerSpeed * gamepad1.left_trigger);
            bottomLeft.setPower(triggerSpeed * gamepad1.left_trigger);
            bottomRight.setPower(-triggerSpeed * gamepad1.left_trigger);
        }

        Double gripperSpeed, slideSpeed;
        gripperSpeed = 0.0;
        slideSpeed = 0.0;

        if (gamepad1.x && !(gamepad1.b)) {
            gripperSpeed = -0.5;
        }
        if (gamepad1.b && !(gamepad1.x)) {
            gripperSpeed = 0.5;
        }
        if (gamepad1.a && !(gamepad1.y)) {
            slideSpeed = 0.2;
        }
        if (gamepad1.y && !(gamepad1.a)) {
            slideSpeed = -0.5;
        }

        if (gamepad2.x && !(gamepad2.b)) {
            gripperSpeed = -0.5;
        }
        if (gamepad2.b && !(gamepad2.x)) {
            gripperSpeed = 0.5;
        }
        if (gamepad2.a && !(gamepad2.y)) {
            slideSpeed = 0.2;
        }
        if (gamepad2.y && !(gamepad2.a)) {
            slideSpeed = -0.5;
        }

        Gripper.setPower(gripperSpeed);
        Slide.setPower(slideSpeed);

        telemetry.addData("Gamepad Left X", gamepad1.left_stick_x);
        telemetry.addData("Gamepad Left Y", gamepad1.left_stick_y);
        telemetry.addData("Gamepad Right X", gamepad1.right_stick_x);
        telemetry.addData("Gamepad Right Y", gamepad1.right_stick_y);

        telemetry.addData("topLeft", topLeft.getPower());
        telemetry.addData("topRight", topRight.getPower());
        telemetry.addData("bottomLeft", bottomLeft.getPower());
        telemetry.addData("bottomRight", bottomRight.getPower());
        telemetry.addData("Speed Multiplier", speedMultiplier);

        telemetry.addData("Gripper", Gripper.getPower());
        telemetry.addData("Slide", Slide.getPower());

        telemetry.update();
    }

    /*
     * Code to run ONCE after the driver hits STOP
     */
    @Override
    public void stop() {

    }
}